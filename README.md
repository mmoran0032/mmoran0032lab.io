# Personal Website

Personal website hosted by GitLab Pages. Owned domains forward here for
the time being.

## Development

The website is deployed using [Material for MkDocs][1]. You'll need to
set up your local environment by installing the package and a few
additional plugins. You can refer to `.gitlab-ci.yml` for the current
set of extensions if this changes.

```sh
python -m pip install \
    mkdocs-material \
    mkdocs-git-revision-date-localized-plugin \
    mkdocs-macros-plugin
```


While editing, you can view a live preview by running

```sh
mkdocs serve
```

The rendered website will be available on [127.0.0.1:8000][3]. After
editing, the static site can be created with

```sh
mkdocs build
```

Originally, this website was developed on GitHub using GitHub Pages. I
am migrating everything that should be maintained to GitLab. In that
migration, I am updating how I manage this site.

Any push to `main` will automatically trigger a build and deploy on
GitLab. The pipeline can be observed in the regular CI/CD panel of the
repo.

[1]: https://squidfunk.github.io/mkdocs-material/
[2]: https://icons.getbootstrap.com/
[3]: http://127.0.0.1:8000
