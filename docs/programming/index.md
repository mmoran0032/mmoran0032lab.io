---
title: Programming
description: details about how I work as a data scientist
---

This collection of resources is geared to how I work in a development
environment in terms of my computer setup, tools, techniques, and so on.
Wherever possible, I'll *try* to discuss topics that relate to my
current or previous work without exposing to many details of what was
actually done, especially if they are sensitive in nature.

For the most part, this will be here to remind me how to do things,
which may be helpful to some people, but there are definitely better
resources for that elsewhere.
