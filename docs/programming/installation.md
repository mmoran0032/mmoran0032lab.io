# Installation

When you get a new computer, or decide to do spring cleaning on a
current computer and wipe-reinstall, follow these instructions. You
started recording this in your [essentials][1] repo, but will be
reproduced here for wider exposure.

The next time you wipe your hard drive or decide to upgrade and need to
start from scratch, this will help you out tremendously. Most of the
re-installation work is in setting up your dev environment, including
editors and your Python environment. For Mac installs, you're mostly
only going to care about `zshrc`, VSCode settings, and
Anaconda/`python`, which currently you'll need to copy over yourself.

## Install History

Log of the installations you've done using this package. For
completeness, the partial Mac installs are also listed below, even
though they don't use the entirety of this repository and are not the
focus of tracking installations.

- 2021-06-07: *Duo Security* Macbook
- 2020-12-29: `ARES-3` (Linux Mint 19 desktop) **reinstall**
- 2020-08-22: `ARES-4m` (Linux Mint 20 laptop)
- 2020-07-19: `ARES-4` (Windows 10 + Windows Subsystem for Linux)
- 2019-02-19: `ARES-3` (Linux Mint 19 desktop)
- 2019-01-15: *White Ops* Macbook
- 2017-12-17: `ARES-2m` (Linux Mint 18 laptop)
- 2017-10-16: *Gartner* Macbook
- 2016-05-03: `ARES-2` (Ubuntu 16 laptop)

Previous installations and refreshes of Ubuntu and Linux Mint were not
recorded at the time, so the log is only complete for recent dates. For
instance, I had repairs done on the `ARES-2*` laptop in July 2014, and
likely did a wipe-and-replace on the harddrive around that time.

## Editors

You primarily use [VSCode][2] for your editing purposes. In the past,
you've also used domain-specific editors (e.g. IntelliJ) when necessary.
Your VSCode configurations are saved and propagated in your `setup.py`
script, including your extension list. Getting that extension list to
show up automatically is a little finicky, so you've copied it into the
workspace file for your `essentials` to get that process to bootstrap
your installation. You can expand `setup.py` to automatically install
those extensions for you, after getting the `code` CLI set up following
installation.

For quick edits, `git` messages, and other small needs, you use `vim`.
Likewise, your Vim configuration is set up using your bootstrap script.
You haven't changed it in a while, so in the future you may want to
think about to what extent it is working for you (if at all). For now,
the persistence is there just in case, but you are using Vim less and
less at this point.

## Things to look into

For Linux Mint, it would be *really* nice if our bootstrapping script
also set up my various preferences so that I wouldn't need to do a bunch
of manual tasks after, like setting the system Terminal shortcut or
changing the calendar date format.

Include more things into the bootstrap script. Could we clone repos from
there? Could we install additional packages? Could we set up `zsh` and
Timeular and `git` and everything else from that? We possibly would want
to convert it back to a shell script, otherwise it would be a ton of
additional code for not much gain.

[1]: https://gitlab.com/mmoran0032/essentials
[2]: https://code.visualstudio.com/
