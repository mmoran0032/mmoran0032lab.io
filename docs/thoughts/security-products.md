# Security products

Collection of various thoughts on building and maintaining security
products to solve problems.

## Building a security product

What should you think about when building a security product? The
original content was located...*somewhere*...but I did not persist the
source. I will attempt to find it later, since the content *sounds* like
it's from someone at Google.

- You should build large-scale AI systems to detect and block threats at
  scale before those threats can reach users. Examples of this working
  are safe browsing, where the user is stopped from reaching a malicious
  site, or identifying phishing emails before the hit the inbox. Pure
  rules-based approaches can't scale to stop all of those threats.
- Keeping up with constantly evolving attacks requires continuously
  improving and retraining detection systems. Otherwise, the adversaries
  eventually can subvert whatever decision boundary is in place. This
  concept is the [Red Queen Hypothesis][1], where you need to adapt and
  evolve to survive since your adversaries are doing the same thing.
- What do you do if the situation is borderline? Give the user as much
  context as possible and rely on them to make the final decision.
  Choices like a warning banner will let them know to be aware, but
  without additional details, they may ignore it. If there are things
  they need to research on their own, are there ways that you could make
  their research easier?
- Over time, there will be more targeted attacks against journalists,
  hacktivists, politicians and campaigns, executives, fintech users, and
  celebrities. Can you make security products that work in those cases
  without getting in the way of their job or life?

I had saved some of these notes months before joining Duo Security, but
the underlying concepts ring really true to what the team has been doing
and what they are trying to get done.

## Universal design

The [Coalesce 2021][2] talk ["Inclusive design and dbt"][3] primarily
discussed how `dbt` and SQL are good cases of [universal design][4] in
data products. Universal design is the idea that designing *something*
to meet the accessibility needs of a few can improve the use for all. A
real-world example are sidewalk cutouts at intersections: designed for
wheelchair use, but helpful for a variety of people and situations.
Additionally, if you don't take the time to intentionally *include*
people, you will unintentionally *exclude* people.

Universal design is characterized by

- *Equitable Use:*
- *Flexible Use:*
- *Simple and Intuitive Design:*
- *Perceptible Information:* communicate information effectively to the
  user
- *Error Tolerance:*
- *Low Effort:* facilitate efficient and comfortable use (e.g. reduce
  boilerplate, minimize repetitive actions)
- *Proportional Size in Space:*

When considering the problem space of what a problem is trying to solve,
I have in the past focused on how *I* would want to solve it versus what
would solve the issue in the best way. That can be an issue when someone
else needs to use the product. Without taking the time to see what the
needs of the users are or how they are using it, the design of the
product will not be as good as it could be.

For security products, this becomes more important as we are trying to
solve a security problem for the users in a way that makes sense. An
overly verbose product with intractable domain-specific information is
not as effective as something that explains or guides the user through
how they should work with the system. Instead of reporting an
explanation code, you can provide a readblae description.

Programming languages have improved how their error messages show up to
improve how new coders work with the language, but that also improves
how *everyone* works with the language.

[1]: https://en.wikipedia.org/wiki/Red_Queen_hypothesis
[2]: https://www.getdbt.com/coalesce-2021/
[3]: https://www.getdbt.com/coalesce-2021/inclusive-design-and-dbt/
[4]: https://en.wikipedia.org/wiki/Universal_design
