---
title: Thoughts
description: collections of ideas and brain-dumpings
---

In general, I try to record ideas down that are related to my personal
life in a way that makes sense and persists across devices. Previously,
I used a private GitLab repo to accomplish this, but after realizing
that I would commonly copy information from that repo to share with
others in a non-technical way, it made sense to start pulling some of
those out and refining them.

What follows are some slightly-organized discussions on things that
matter to me in some professional way but that don't directly relate to
the explicit code that I write. They contain ideas that I currently do,
things I'm trying to do more of or more appropriately follow, or things
that I want to keep in mind for the future. Where applicable, I'll link
to a resource that spurned the idea in the first place.
