# High-functioning remote teams

There was a [Twitter thread][1] discussing some learnings about how a
good fully-remote team works. There are some really interesting ideas
within that thread that I want to discuss more.

## Direct messages

!!! quote
    We almost never DM! We have a team channel that’s just the 9 of us
    and we use it to talk about work and also socialize a bit—--like a
    mission-focused group chat. Any observations or questions get put to
    the group, reducing knowledge silos

I've found that this is one of those things where a fully-open culture
can be extremely helpful so long as there is a culture around it. I
think that focused team chats make a ton of sense, and using them for a
mix of uses makes sense as well. I need to get better with starting
conversations in the open. I have had trouble reducing the feeling of
"bugging" everyone when I post something within a team channel, so that
should be a goal of mine for the near term.

I think that I can set a goal for myself to not use any DMs except when
they are actually intended as a "private" message.

## Written records

!!! quote
    This goes back to our written communication being very important.
    Because our communication is asynchronous, written records matter a
    lot. [...] Like DMs, meetings that only two people know about are
    not encouraged. If two people set an impromptu meeting to discuss a
    feature or bug fix, they post about it in main chat so others can
    join to learn or offer advice. Attendance is optional though—--we
    want to avoid zoom fatigue

I pride myself on having written records, but my written records are not
the best that they can be. I know that I need to improve my concision
and targeted communications to the audience. I will have plenty of
opportunities for that in the coming months. With respect to meetings,
there aren't too many times where there are only single meetings. I feel
like most of them are pre-planned and scheduled with the entire subteam.
I think that one thing to try would be using Slack Huddles, which gives
the notice to everyone else that the meeting is going on without being
intrusive to their work.

## Socialization

!!! quote
    Our meetings have a bit of optional time at the end to socialize and
    catch up. We’ll also occasionally have a “coffee hour” in someone’s
    meeting room just to chat. Like all meetings that aren’t on the
    calendar, these are optional. But they’re nice

I've really liked the built-in social time in recent recurrent meetings,
as well as some of the ad-hoc chatting. I've felt like it has helped a
lot in improving my connection with the team, and I've always enjoyed
taking part in them. I wonder if there are other ways in which I could
have fewer boundaries to other people socializing with me, which would
get around some of my mental hurdles.

## Closing thoughts

There are other remote work and culture discussions around the place,
and I probably have a handful saved that could be rolled into this one.
It takes more deliberate effort to have a good culture in a remote-only
environment, which means that *I* need to be deliberate about it as
well.

[1]: https://mobile.twitter.com/buttpraxis/status/1399057827063238657
