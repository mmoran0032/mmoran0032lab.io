# Teams own problems, not products

This idea was brought forward on [Twitter (@DynamicWebPaige)][1], where
"teams should consider themselves as owning *problems, scenarios, or*
*use cases*, not owning products. That reframing is because a product,
language, or any other *thing* can be transient, but the problem
generally is not transient.

Sometimes, it's easy to conflate the two because of the tight linkage
or the lineage of the product, as well as the evolution of that product
as the knowledge about the problem changes. This is maybe a little bit
like the [Ship of Theseus][2] where you keep changing the "product" to
match the problem such that over time none of the original "product"
remains. You're still solving the same problem, so focus on solving
*that* versus building a product.

[1]: https://mobile.twitter.com/DynamicWebPaige/status/1392578015033430016
[2]: https://en.wikipedia.org/wiki/Ship_of_Theseus
