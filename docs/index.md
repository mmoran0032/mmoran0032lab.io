# About

<figure markdown>
  ![Mike Moran headshot in orange tint](assets/mikemoran-orange.png){ width=350 }
  <figcaption></figcaption>
</figure>

I am a Senior Data Scientist at [Duo Security][1] on the Algorithms team
working to enhance how we provide simple, powerful access security by
improving our threat and anomaly detection methodology. I've previously
worked at [Human Security][2] (née White Ops) identifying and stopping
bot activity impersonating Connected TV devices (e.g. smart TVs,
streaming devices/services) to commit ad fraud, and at [Gartner][3]
building their internal document search ranking models.

My previous life had me complete a Ph.D. in experimental nuclear
astrophysics at the University of Notre Dame's [Nuclear Science Lab][4]
and earning a B.S. in astrophysics and a B.S. in physics from Michigan
State University's [Lyman Briggs College][5].

My current interests include anomaly detection in the security space,
and creating usable products from those detections. My
multi-disciplinary background supports my creative problem solving (ask
me how I stopped multiple botnets using astrophysics), and I am always
willing to learn more from the people and teams around me.

[1]: https://duo.com/
[2]: https://www.humansecurity.com/
[3]: https://www.gartner.com/
[4]: https://isnap.nd.edu/
[5]: https://lbc.msu.edu/
