import altair as alt
import numpy as np
import pandas as pd
from sklearn.datasets import make_classification

np.random.seed(20150808)

X, y = make_classification(
    n_samples=100,
    n_features=2,
    n_informative=2,
    n_redundant=0,
    n_repeated=0,
    n_classes=2,
)

df = pd.DataFrame({"x": X[:, 0], "y": X[:, 1], "label": y})

base = alt.Chart(df).encode(x="x:Q", y="y:Q")

chart = base.mark_circle(size=50).encode(color="label:N")

nearest = alt.selection(type="single", nearest=True, on="mouseover", fields=["x", "y"])


# Step 3: Transparent selectors across the chart. This is what tells us
# the x-value of the cursor
selectors = (
    base.mark_point()
    .encode(
        opacity=alt.value(0),
    )
    .add_selection(nearest)
)

rules = base.mark_rule(color="gray").transform_filter(nearest)

chart + selectors + rules
