import argparse
import random
import shutil
import time
from typing import Any, Dict


width = shutil.get_terminal_size((80, 20)).columns


def parse_arguments() -> Dict[str, Any]:
    p = argparse.ArgumentParser()
    p.add_argument("--lines", type=int, default=20)
    args = vars(p.parse_args())
    return args


def diag_art(lines=20):
    chars = ["\\", "/"]
    for _ in range(lines):
        newline = "".join([random.choice(chars) for _ in range(width)])
        print(newline)
        time.sleep(0.1)


if __name__ == "__main__":
    args = parse_arguments()
    diag_art(**args)
