
# Estimating project sizes

Estimating sizes is difficult. We can think about two different ways to estimate a project size: time and risk/complexity.

## Time

*Adapted from "Clean Coder"*

Timeline estimates should be based on a trivariate analysis: an
optimistic, nominal, and pessimistic estimate.

- **Optimistic** should be wildly optimistic, where everything goes
  right and there are no other hiccups. This should be roughly a
  1-in-1000 chance of actually happening
- **Nominal** should be the greatest chance of success, so at the peak
  of a bell curve for the outcomes.
- **Pessimistic** should be wildly pessimistic, where everything goes
  wrong and everything gets in the way (ignore natural disasters here).
  Again, there should be a roughly 1-in-1000 chance of this happening.

Taking these time estimates, you can figure out roughly how long the
task will take from the following equations. The expected duration of
the task is

    mu = (O + 4N + P) / 6

while the uncertainty of that expected duration is

    sigma = (P - O) / 6

Suppose the estimates are `O = 1`, `N = 3`, and `P = 12`, then the
expected duration is `4.2 +/- 1.8` days. In most cases, this total
estimate will be somewhat pessimistic since the pessimistic tail of the
estimates is longer than the optimistic one.

With multiple tasks, you can still find the expected duration. You take
the expected durations of each individual task and add them; this is
your total expected duration. The uncertainties must be added in
quadrature, so

    sigma_total = sigma_1**2 + sigma_2**2 + ...

What this estimation scheme does is add *realism* to the estimate. Since
only relying on optimistic estimates can harm your reputation (In the
example above, it is very likely for the task to take *six times* longer
than the optimistic estimate) by appearing to always be finished later
than you think. If you are always late, you can't be trusted to finish
anything on time, and anything else that is relying on your portion
being on time will also be delayed.


## Risk and complexity

*Adpated from [Setting expectations as a product designer][1]*

We can estimate the *risk* and *complexity* of a project to obtain approximate sizes. We'll rate each of these on a scale of 1-3. For our two metrics, we'll define them as

- **risk:** what would be the impact of an unsuccessful solution on the product or company?
- **complexity:** what is the expected effort, including research, to get this done?

Neither of these directly reference time, but the combined value indirectly references them. From the two scores above, we simply add the values we assigned to map to a "T-shirt" size:

| XS | S | M | L | XL
| --- | --- | --- | --- | --- |
| 2 | 3 | 4 | 5 | 6 |

Over time, capacity (i.e. what "total size" the individual or team can handle) can be clarified. For example, you may be able to accomplish one XS task per week around all of your other responsibilities. Or maybe there is no way to complete an XL project in a single quarter, so you need to break it down more.

[1]: https://medium.com/@cattsmall/setting-expectations-as-a-product-designer-b8584e8fb138?_branch_match_id=829013808876505242