# Anomalies

Content below is quoted from [Deep Learning for Anomaly Detection][1]
(Fast Forward Labs report). Eventually I'll write up my own thoughts on
this so that it's in my own voice.

For the training approaches discussed thus far, we operate on the
assumption of the availability of “normal” labeled data, which is then
used to teach a model recognize normal behavior. In practice, it is
often the case that labels do not exist or can be expensive to obtain.
However, it is also a common observation that anomalies (by definition)
are relatively infrequent events and therefore constitute a small
percentage of the entire event dataset (for example, the occurrence of
fraud, machine failure, cyberattacks, etc.). Our experiments (see
[Chapter 4. Prototype][2] for more discussion) have shown that the
neural network approaches discussed above remain robust in the presence
of a small percentage of anomalies (less than 10%). This is mainly
because introducing a small fraction of anomalies does not
significantly affect the network’s model of normal behavior. For
scenarios where anomalies are known to occur sparingly, our experiments
show that it’s possible to relax the requirement of assembling a
dataset consisting only of labeled normal samples for training.

Machine learning models that learn to solve tasks independently from
data are susceptible to biases and other issues that may raise ethical
concerns. Anomaly detection models are associated with specific risks
and mitigation tactics. In anomaly detection, when the definition of
“normal” is independently learned and applied without controls, it may
inadvertently reflect societal biases that can lead to harm. This
presents risks to human welfare in scenarios where anomaly detection
applications intersect with human behavior. It should go without saying
that in relation to humans, one must resist the assumption that
different is bad.

[1]: https://ff12.fastforwardlabs.com/
[2]: https://ff12.fastforwardlabs.com/#prototype
