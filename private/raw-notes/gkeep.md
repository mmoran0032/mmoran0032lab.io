# Google Keep notes

Copied over from Google Keep to move stuff off of that platform. For the notes, if they are long, they
are in their own section, while short notes are just a single point in a list.

## Short notes

- "Dead weight of a wet blanket": Laura's memoir title
- You can only change so much on your own, and if other people don't change, that's not on you.

## Security

Build large-scale AI powered systems to detect and block threats at scale before they reach users

- e.g. safe browsing, phishing

Keeping up with constantly-evolving attacks requires continuously improving and retraining detection systems

- Red Queen Hypothesis: must adapt and evolve for survival

Borderline cases? Provide as much context as possible and rely on the user to make the final decision

Targeted attacks against journalists, hacktivists, politicians and campaigns, executives, fintech users, and celebrities
