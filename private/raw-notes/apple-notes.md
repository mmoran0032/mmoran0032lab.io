# Apple Notes

Copied over from Apple Notes.

## LEGO

- "Det bedste er ikke for godt": only the best is good enough (Dutch)
- "Leg godt": play well (Dutch)
- "Lego": I put together (Latin)

## Story

A large spacefairing coalition of races comes to Earth to recruit pet owners to be representatives of Earth in the coalition. They want pet owners because those peoploe are used to communicating and showing care and empathy toward things that are not their own race.

## Professionalism thoughts

A coworker told me today that 'teams should consider themselves as owning *problems, scenarios, or use cases*, not owning products because products, languages, etc., can be transient, but problems generally aren't. [Twitter][1]

All of the parameters in a configuration file are experiments waiting to happen. [Podcast (26:00)][2]

[1]: https://mobile.twitter.com/DyanmicWebPaige/status/1392578015033430016
[2]: https://twimlai.com/causal-models-in-practice-at-lyft-with-sean-taylor
