import io

import numpy as np
from pytest import fixture

from gaussian import Gaussian


@fixture
def g():
    return Gaussian(25, 2)


def test_initialization(g):
    assert g.mean == 25
    assert g.stdev == 2


def test_repr(g):
    assert repr(g) == "Gaussian(mean=25,stdev=2)"


def test_pdf(g):
    assert np.isclose(g.calculate_pdf(25), 0.19947)


def test_mean_calculation_sampled(g):
    f = io.StringIO("""1\n3\n99\n100\n120\n32\n330\n23\n76\n44\n31""")
    g.read_data_file(f, sample=True)
    assert np.isclose(g.stdev, 92.874598)


def test_mean_calculation_unsampled(g):
    f = io.StringIO("""1\n3\n99\n100\n120\n32\n330\n23\n76\n44\n31""")
    g.read_data_file(f, sample=False)
    assert np.isclose(g.stdev, 88.552454)


def test_gaussian_add():
    g1 = Gaussian(5, 2)
    g2 = Gaussian(10, 1)
    result = g1 + g2
    assert np.isclose(result.mean, 15)
    assert np.isclose(result.stdev, 2.236068)
