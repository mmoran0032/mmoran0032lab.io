# Lesson 03: Software Engineering Practices, Part 2

When we consider best practices, writing and tracking code is only one
part of what we'll need to do. How do we know the code works? How can we
provide information about the code that's running to inspect it to make
sure that it is still performing as desired? How can we make sure that
we wrote code that the rest of the team understands? These questions are
answered by this lesson, and generally add *robustness* to our code.

## Testing

Writing and running tests allows us to be confident that the code is
working as expected, preventing us from making conclusions on faulty
data or logic, and preventing a failure in a production system. We can
test both the structure of the code, as well as our underlying
assumptions about the code and the data, such as string encoding,
unexpected features or scores, or faulty conversion logic.

We can use *test-driven development* to help organize the code testing,
where we write a unit test before writing the production code. We'll
write a single test, make sure that our code fails, then write the
minimum required code to get the test to pass. At this point, we add
additional test cases to the suite, plus update the logic of our code,
to make sure things are working as expected.

We'll be using `pytest` for our test suite. In most cases, the code is
clearer than using `unittest`, and in cases where more complexity is
needed, the system of including that complexity is nicer. In general,
`pytest` is a more common standard for Python testing. Depending on the
actual tests, we can use it for only unit tests, or also *integration*
*tests*, which make sure the end-to-end running of the code works.

### Test-driven development

Our process for test-driven development (TDD) is as follows:

- write a test, run test, ensure that it fails
- write or update the tested function to get the test to pass
- include additional test cases to test other cases

You may also write a bunch of tests first as you think about what edge
cases may pop up (e.g. an email address with multiple `@` symbols), and
then repeatedly run your test suite as you work on the function. This
process is fine, since we've written our tests first.

When testing code, [Hypothesis][1] is a good framework for
property-based testing, which may show up more frequently in data
science code, where we want to check to make sure our features are
strings, or the values fall within certain ranges. Hypotheis has
built-in strategies for `numpy` and `pandas`, which can be very helpful.

## Logging

Creating logs is essential for ML models in production, since thet are
sometimes the only way to know how a model is performing, especially
when we aren't actively watching the output. Different logging systems
are useful here, and some have been created just for ML models.

When creating logging messages, we want to do the following:

- be professional and clear
- be concise and use normal capitalization
- choose the appropriate level (DEBUG, ERRO, INFO) for logging
- provide any useful information

## Code reviews

Code reviews require an additional person to look at your code. They are
useful to catch errors, ensure readability, check that standards are
met, and share knowledge among the teams. Some examples of data science
specific code review topics would be around how the features are being
used or defined, appropriate logging, and others. In general, a code
review will ask about everything we've discussed so far. See
`code-review-questions.md` for specific questions.

When writing and reviewing code, there are a few helpful things to
consider:

- **use a linter:** most editors have a default linter, and you'll
  likely also want to post-process the code (e.g. using `black` for
  Python code) to keep things consistent
- **explain issues and make suggestions:** adding in extra details,
  instead of just saying "change this" will help the updates go through
  more smoothly
- **keep the comments objective:** the review should be about the code,
  not the person writing it
- **provide code examples:** it can be helpful to provide a `diff` or
  example code for some changes

[1]: https://hypothesis.readthedocs.io/en/latest/

