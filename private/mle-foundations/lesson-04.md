# Lesson 04: Introduction to Object-Oriented Programming

We're going to build a Python package as a part of this lesson. I have
done this to some extent before, but do not know modern best practices.
There are a few additional resources that I can use to help figure out
what the modern creation and publishing practices are, but this should
work well too.

## Procedural versus OOP

Objects are defined by their *characteristics* (attributes) and
*actions* (methods). When we work with objects, we call methods on an
instantiated object that produce some output, outcome, or adjust the
state of the object by adjusting the attributes of the object. For
example, models in `scikit-learn` are objects that we can use:

```python
from sklearn import svm

model = svm.SVC()
model.fit(X, y)
```

The object itself is a particular instance of a *class* of objects. For
example, our `model` is an object of the `svm.SVC` class.

### Example class

We'll write up an example class. The course does not use `@property` and
`@attr.setter` for attribute access and changing. We'll override that in
our code.

```python
class Shirt:
    def __init__(
        self,
        color: str,
        size: str,
        style: str,
        price: Union[int, float]
    ) -> None:
        self.color = color
        self.size = size
        self.style = style
        self._price = None
        self.price = price

    def __str__(self) -> str:
        return f"color: {self.color}, size: {self.size}, style: {self.style}, price: {self.price}"

    @property
    def price(self) -> Union[int, float]:
        return self._price

    @price.setter
    def price(self, new_price: Union[int, float]) -> None:
        self._price = new_price

    def discount(self, deduction: float) -> float:
        """display a discounted price based on [0-1] deduction"""
        return self.price * (1 - deduction)
```

We don't have docstrings above, but could easily include them (and we
should in a real set of code). The code abovee is very simple, so we
don't need to worry about it, but including those docstrings will help
with actually writing the code that uses it, since the IDE will provide
the docstrings as contextual help while we're working.

We'll write out a better class in our code space, instead of our notes.

## Inheritance

There is a powerful part of OOP that can make code more maintainable and
extensible: *inheritance*. In this case, a base or parent class is used
to define common functionality of a bunch of different subclasses. For
example, a predictor in `sklearn` inherits from the `BaseEstimator`
class which defines some base functionality. For these base classes, we
can use the `NotImplementedError` except to show where the dependent
classes *must* override the functionality.

## Special methods

Beyond just methods defined within a class and used by the object, we
can also create additional methods that work differently:

- `classmethod` can be called either from an object or the class, but
  can change only the state of the class, no an individual instance
- `staticmethod` can be called from an object or a class, but cannot
  change the state of either

For example, `sklearn` base estimators define a class method called
`_get_param_names` that returns the parameter names for a given class
(e.g. SVM). Since those names depend on the class, and not any given
instance, we can define it as a class method.

## Creating a package

The course provides information about how to make and install a package.
I'll be going through the code and work in the course. Jacob Tomlinson
has a series of posts about making a Python package, starting with
[making the package][1], which covers code, version control,
requirements, licensing, and `setup.py`. The publishing portion of the
series talks about what is necessary for PyPI *and* Conda. Follow that
when you actually need to do this work.

[1]: https://www.jacobtomlinson.co.uk/posts/2020/creating-an-open-source-python-project-from-scratch/

