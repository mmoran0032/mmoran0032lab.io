# Lesson 02: Software Engineering Practices, Part 1

We're starting with software engineering, which is a mix of how to write
and think about code so that it is usable, maintainable, and easier to
understand by other people. We want to write production code, which will
run potentially continuously, and doing that is a way that is easy to
update, fix, and reason about it essential.

## Clean and modular code

Our production code will be running on servers that will handle live
data and users. The code should be reliable, efficient, and performant.
In general, the code should also be *clean* (readable, simple, and
concise), which will help with maintainance and collaboration.

Modular code allows us to be ready for production and be clean. The code
is broken up into functions and modules, allowing the code to be more
organized, efficient, and reusable. This lets us reuse our code easier,
write less code when starting something realted, collaborate with other
people on your team, and read and understand the code.

## Refactoring

The first code we write will likely not fit the above. To get our code
to fit the requirements above, we want to *refactor*, which is rewriting
our code to improve its internal structure without changing its external
results. Refactoring is a great opportunity to improve your coding
ability, allowing you to spend less time in the future on writing code
that needs to be extensively refactored.

## Writing clean code

*See* Clean Code *by Martin Fowler for more, plus my own notes.*

Clean code can be described by a few simple requirements:

- **meaningful names:** meaningful or descriptive names are helpful to
  read the code without relying on comments. The descriptions may imply
  the type of the variable or return parameter, they should be
  consistent but differentiable, and should avoid abbreviations and
  single letters (except for a few exceptions). If those variables are
  common knowledge, then shorter names are fine. Remember that longer
  names don't necessarily mean more meaningful names.
- **use whitespace properly:** while this is less important with Python
  since the language requires it, you should still think this through.
  Of course, running `black` over the code is extremely helpful for
  using this, and you probably should have `black` as a pre-commit hook
  for your code.

## Writing modular code

Modular code can also be considered clean, but we'll discuss it
separately. Our modular code can help with comprehension of the actual
code, and help with reuse. We want to do the following:

- **don't repeat yourself (DRY):** when you have identical code, you
  should abstract it out into a function or module to use instead, or
  with loops
- **abstract logic:** if you have similar code, you should look for the
  similarities and create code objects that can be reused with
  parameters.
- **minimize the number of entities:** more functions and modules
  doesn't mean that the code is better. The total number of code objects
  you create should be kept to a minimum.
- **functions should do one thing:** if the function name contains
  *and*, it's likely doing too many things.
- **arbitrary variable names can help comprehension:** using similar and
  abstracted names can help with comprehending the entire code base, not
  just a single function, method, or module.
- **limit the number of arguments:** try to use three or fewer
  arguments. In some cases, this might be unavoidable (e.g. `sklearn`),
  but for code that you write, you should be able to find ways to limit
  the number of passed parameters.

## Efficient code

When we refactor code, we want to think about our *efficiency* of our
code. Our code is efficient if we've minimized the time for the code to
run, and the memory space required to run the code. In some cases, we
don't need to worry about the speed of the code if the code doesn't run
frequently or not in a live environment (e.g. batch data processing).

We can get efficient code in a number of different ways, depending on
the actual code itself. Some tips:

- **use vector operations where possible:** use `pandas` and `numpy`
  where possible. You'll likely need to do some searching through the
  API of each to find the right thing.
- **know data structures:** you should use the right data structures for
  the right tasks, for example using sets over lists to find common
  elements.

## Documentation

Additional text to describe the code is necessary, especially when
complex parts need to be clarified. We can use different types of
documentation depending on the need, like inline comments, docstrings
for functions or modules, and project-level documentation like
`README`s.

## Version control

Keeping track of versions of your code and models is very helpful for
monitoring and tracking work through time. The version control for the
created models is likely to be different than the code that produces the
models. I've been using `git` for code version control for over a
decade. I have a lot of the basic commands memorized, but I usually need
help for some commands. The built-in aliases in `zsh` help out, like
`gpsup` for "git push --set-upstream origin $CURRENT_BRANCH". Of course,
your IDE of choice (*Codium*, based on *VSCode*) has built-in `git`
integration, so there's less reliance on memorizing shell commands to
use `git` properly.

Tracking changes in models and data is harder. There are multiple ways
to track these changes, both through `git` (configuration file, model
reports, etc.) as well as special purpose systems. Each team will likely
have their own way to do this, but you should think about a good system
for you personal projects.

