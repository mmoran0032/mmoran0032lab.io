from typing import List

from matplotlib import pyplot as plt
import numpy as np


class Gaussian:
    def __init__(self, mean: float = 0, stdev: float = 1):
        self.mean = mean
        self.stdev = stdev
        self._data: np.ndarray = None

    def __repr__(self) -> str:
        return f"Gaussian(mean={self.mean},stdev={self.stdev})"

    def __add__(self, other: "Gaussian") -> "Gaussian":
        return Gaussian(
            mean=self.mean + other.mean,
            stdev=np.sqrt(self.stdev ** 2 + other.stdev ** 2),
        )

    @property
    def data(self) -> np.ndarray:
        return self._data

    @data.setter
    def data(self, new_data: np.ndarray) -> None:
        self._data = new_data
        _ = self.calculate_mean()
        _ = self.calculate_stdev()

    def calculate_pdf(self, x: float) -> float:
        """calculate the probability density function at a value"""
        return (1 / np.sqrt(2 * np.pi * self.stdev ** 2)) * np.exp(
            -0.5 * ((x - self.mean) / self.stdev) ** 2
        )

    def calculate_mean(self) -> float:
        self.mean = np.mean(self.data)
        return self.mean

    def calculate_stdev(self, sample=True) -> float:
        ddof = 1 if sample else 0
        self.stdev = np.std(self.data, ddof=ddof)
        return self.stdev

    def read_data_file(self, filename: str, sample: bool = True) -> None:
        self.data = np.loadtxt(filename)
        _ = self.calculate_stdev(sample)
