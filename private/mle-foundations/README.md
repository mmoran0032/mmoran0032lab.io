# aws-ml-foundations

Personal course materials for the Udacity course
[AWS Machine Learning Foundations][1], starting on 2020-05-22. Since my
engineering skills are mostly ad-hoc, I want to actually get some
directed learning for this component. This course will help me be a
better engineer, give me a good outlet for actual code development, and
if necessary allow me to change my role or job in the near future.
Finally, I want to see what the current state of affairs is in the
industry, since my work at White Ops has not been as ML-focused as it
could otherwise be, but I will be working on changing that.

[1]: https://classroom.udacity.com/courses/ud090
