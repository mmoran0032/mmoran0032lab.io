# Git Basics

Information on using `git`, especially for common tasks that I
frequently forget how to do.

## Change primary branch to main

In Git 2.28.0, there is a configuration option to set the default branch
name of new repos. You can enable this config from the command line with

```bash
git config --global init.defaultBranch main
```

When pushing an initialized repo to a new logged repo on Git(Hub|Lab),
and when doing any branch manipulation, you will need to use the new
default branch name.

## Check in an empty directory

Technically, you cannot do this, and in general it is not recommended
except in some edge cases (like when cloning a template directory). If
it is absolutely necessary, you need to create a placeholder files
within the directory, such as `.keep` or a `README.md` that explains the
purpose of the directory.

## Deleting branches

When deleting branches, you can delete local and remote branched. Local
branches are deleted with one of the following:

```bash
git branch -d branch-name
git branch -D branch-name
```

The second option force deleted the branch, allowing it to be removed
even if it hasn't been completely merged into the default branch.

Deleting a remote branch is a push into the remote repo:

```bash
git push origin --delete branch-name
```
