# Practical Advice for Large Data Analysis

Information compiled from the [Unofficial Google Data Blog][1]. Given my
experience in Search (the author led that team at Google) and at White
Ops, I'll augment the notes and points presented in the blog with my own
thoughts and comments. I was linked to the blog post through the "Rules
of Machine Learning" document I have on GDrive that I downloaded from
somewhere.

The advice in the blogpost is organized into three main areas:
*technical*, *process*, and *social*. I'll copy that same breakdown here
for clarity.

Note that since the original blogpost was written in 2016 (based on
experience over the previous 11 years), there may be some parts that we
would want to update to modern methods or ideas. I don't expect that to
be necessary across most of the document, but it is a possibility, and I
will note those cases where applicable.

In some cases, it would make sense to develop a formal checklist for the
traffic segments under my purview, such as Roku traffic. I think
building that out will be a long-term goal, and would help improve the
team an onboard additional people.

## Technical

This section contains ideas and techniques for how to manipulate and
examine your data. These general guidelines can be thought about as a
basic checklist for any new analysis, and should be codified as such as
much as possible. Note that we are not describing particular statistical
tests or models to build, but instead the analytics routes to take.

### Look at your distributions

Distributions will be anything like histograms, CDFs, Q-Q plots, or
any graphical representation of a feature or features of the data. These
plots are helpful beyond summary statistics (see Anscombe's Quartet or
the modern [Datasaurus Dozen][2]), but we should take care to make sure
the plots themselves are also beneficial. If we are creating a scatter
plot, we're fine, but if we are creating a plot that collapses the data
down too much (such as a box-and-whiskers plot), we may still run into
issues.

When creating these plots, we're looking for the general shape to see if
there is any multi-modal behavior, significant outliers, skew, etc. All
of these results can help inform how you work with the data, and if you
need to select subsets of data to investigate further.

### Consider the outliers

Depending on your target, your outliers might be exactly what you are
trying to find, or they are potentially misleading results. In either
case, you need to examine them to see what they are doing in order to
decide what *you* should do about them. In most cases, these outliers
will be built from some underlying statistic(s) that you've calculated,
plus an understanding of what the "usual" behavior of the data is.

Within White Ops, our outliers are *usually* the bots that we're trying
to find, so taking extra care to figure out what the outliers are doing
is your job. At the same time, we want to avoid operational outliers,
like Roku/Telstra Australian traffic which looks like an outlier when
compared to US traffic until you understand the data generation process
that produced the outliers.

### Report noise or confidence

Always include some sort of uncertainty estimate, confidence level, a
range of values, something that will help people understand that the
actual numbers are not exactly what you state. In other words, never
provide a point estimate on its own. At White Ops, we currently have a
bad habit of providing point estimates on their own, which in some cases
is non-ideal (e.g. providing estimates on future fraud when deploying a
new marker). We should think about ways to get confidence intervals in
an easy fashion, such as by bootstrapping the interval. Since we also,
by default, use a 100x downsample on our data, we could develop some
approximate confidence levels from the volume and the size. In any case,
we should think about this more.

Providing these confidence levels should not be used to provide more
weight to an analysis than you should. For instance, if you calculate
your confidence interval to be extremely small, you likely have an issue
with your assumptions. A good start would be to use weekly volumes, but
aggregate by day or hour and use that as your uncertainty.

### Look at examples

Whenever you run an analysis, you need to look at samples from the data
individually to understand what is going on. These samples should be
taken from the entire distribution, not just the most common part of the
distribution, in order to gain confidence that you're interpreting the
entire dataset properly. If you think of it like an audit, you can
assess 5-10% of the samples from across the distribution to ensure you
are interpreting things right.

When looking at samples, check the entire feature space for that example
and compare to the previously-compiled distributions. Is the example an
outlier in one dimension? Are there known correlations that could throw
off some of the results? Is there something weird in that particular
sample that you are misinterpreting?

If you have a model, assessing a random set of samples can also help
provide additional support on the precision and recall of the model,
especially if you don't have training data. When assessing these
samples, you need to record your assessment such that you and your model
can learn from the information.

Of course, having a final hold-out set in these situations is extremely
helpful and necessary as to not bias yourself or the model to your
results.

### Slice your data

When exploring your data, you should break it into subgroups to
determine if the phenomenon you are observing is different or consistent
across these subgroups. Depending on what you are expecting to observe,
it's possible that the subgroups will show dramatically different
results. In those cases, your understanding of the phenomenon will guide
if there is something actionable within that slice.

An important thing to consider is "mix shifts," where the amount of
traffic across the two groups are different. If this is the case,
something similar to Simpson's paradox may arise, where the
directionality may flip when looking at the entire population versus a
small subgroup. I *think* this was the problem in the
`firetvImproperHash` marker, where it appeared to have a higher impact
on Fire TV TV Edition models, but there likely was a problem with the
difference in volumes (i.e. the TVs had a lower volume, so had a higher
expected bot rate, which looked suspicious but was likely fine given the
higher volumes of Fire TV Sticks).

### Consider practical significance

When looking at data, the amount of data within a certain slice or
subset is important. If the particular slice is a tiny percent, or the
difference between two slices is extremely small, it's likely not worth
the time to delve deeper. Of course, when your target is to identify and
understand those small sections of data, like finding fraud, then the
reverse is kind of true: ignore the large sections of data without
going through that part of the traffic as much.

Since White Ops' data is large by any sense of the term, you want to try
to process as much as possible. Tools to help process the "bulk" and
highlight the small portions of traffic that deserve further
investigation are essential, as well as the processes to actually make
those assessments.

### Check for consistency over time

You should always try to check results through time. It's common for the
work to be a bit short-sighted based on the volume of data, but if
possible, check across multiple time periods, like days, in recent data.
This process is one area where White Ops does not have that much tooling
in place, beyond dashboards. In some cases, that might be enough, but
for live marker creation, it becomes harder to anticipate changes
without an active beta process for the results.

These checks can also help determine your own confidence in the results
and errors within the data and process. Since we are actively changing
the environment that our data lives in, and thus the data generation
process, there is sometimes a large amount of variability in the data
from day-to-day *after* a threat is found, but usually not before.

## Process

This section contains recommendations on how to approach your data, what
questions to ask, and what things to check. These processes are separate
from the particular things to check, but are ways to collect and
organize how to go through multiple aspects of the analytics.

### Separate validation, description, and evaluation

We can split initial investigation and analysis work into three stages:

1. **validation or initial data analysis:** we want to check that the
   data is what we expect, and was generated in the way we expect
1. **description:** we work to understand an objective of the analysis,
   which at White Ops is usually "is this traffic fraud?", which might
   be answered in a number of different ways
1. **evaluation:** from our description, we need to come to an answer,
   which is usually a yes, no, maybe, or requires further investigation

I expect that the process will be cycling through steps 2 and 3 above,
where as we slice and investigate the data, we'll end up spinning out
other analyses on those slices. Our *evaluation* stage will involve
putting much more meaning on the data, and requires the ability to
communicate that additional meaning in a way that makes sense and can
convince others that your conclusions are valid.

### Confirm the experiment or data collection setup

This step is more important for situations where we may be in control of
the entire data collection process. In the case of White Ops, we have
almost all of these steps under control for us. A possible case would be
if there are integration issues with new clients, like if some parts of
the provided data is missing or malformed. Checking those consistencies
is helpful when the data format may be changing rapidly.

Other cases where the "setup" might change are different environments or
devices, as the produced data might be different (e.g. some fields
missing, some data available).

### Check vital signs

There are set numbers or metrics that should always be checked when
going through the data, related to how you're set up your experiment or
what the provided data is representing. These metrics are mostly
volumetric: has the total amount of traffic changed recently? The amount
of traffic might be shifts, new customers, old customers changing their
traffic, traffic shifting to other areas, etc. Some of these changes
might also be indicative of fraud, but not necessarily.

### Standard first, custom second

When checking changes, always reference old or in-place metrics first
before diving into the new metrics. Again, this piece of advice is more
important in product areas and less so at White Ops. As you develop new
metrics, comparing against the old metrics will help you understand if
the new ones are well-behaived.

### Measure twice, or more

You should always try to measure an underlying phenomenon in multiple
ways. For fraud detection, we usually refer to "orthogonal signals,"
which are distinct from the ones we're using to create the new fraud
detection. We usually construct a marker in reference to the user-agent
string (either in full or from components extracted from it) and some
sort of network signal. If we have multiple measures for fraud, we can
be more sure that the one that we deploy is indicative of fraud.

### Check for reproducibility

In general, we want to check for consistency and stability for what
we're building. This advice matters more for building a model and less
so for a feature (which is what our markers effectively are), but it's
still important to figure out if the model will behaive wildly different
under different conditions. When we build markers for *MediaGuard*, this
advice comes more into play, as each marker is effectively a very
limited statistical model. If something is off or changing too
dramatically as different inputs are changed, like the day its running,
then there's a possibility that the underlying phenomenon has not been
adequately captured.

### Check for consistency with past measurements

When you calculate a metric, you should always compare to what was
measured in the past, from other markers or data sets or tracking
metrics. Where this is more important is around various tracking or
identification metrics, like how much of our traffic is Roku or CTV.

### Make hypotheses and look for evidence

When investigating data, you should make a hypothesis for the phenomenon
you are seeing, then seek to confirm or refute that hypothesis. For
example, if you think some subset of data is fraud, look for support
beyond just what you've currently found, like if there are indicators
that are present elsewhere, or if there is some other anomalous activity
operating in the same space, or if the company itself is suspicious.

We want to make sure that the overarching narrative of the analysis is
leading to a correct conclusion. When supporting this narrative, you
make other future narratives stronger based on your knowledge and
experience, which is a benefit on its own too.

### Exploratory analysis benefits from end-to-end iteration

When running an analysis, try to go through the entire loop multiple
times, where each iteration some aspect might change. By doing so, you
can reinforce the narrative you are telling, build a better story or
identification of the underlying aspect, build up better systems for
running the same or similar analysis, and more. The initial focus should
be on getting something decent, with notes for yourself for future
improvements, so you can quickly determine if the track you're going
down is the right path.

## Social

This section contains notes on how to work with others and communicate
about your data and insights.

### Data analysis starts with questions, not data or a technique

Take the time to formulate your needs as questions or hypotheses before
starting the work. Doing so will help you better know what data you need
to collect, what gaps there might be, and direct your overall work.
Without questions, your work will be a little aimless. These questions
can and should evolve as you understand the problem more, with analyses
leading to more questions to further the work.

### Acknowledge and count your filtering

When subsetting the data, you should aim to clearly specify what you are
doing with the filtering and how much is being filtered out at each
step. For example, if you are selecting only particular customers' data,
how much other data remains? You should also look at examples of what is
being excluded and included to make sure your filtering it correct.

### Ratios should have clear numerator and denominator

Since most interesting metrics are ratios of underlying measures,
specify what those underlying measures are even if the ratio is common.
This is essentially especially when comparing to previous results,
further interpretation, or just referencing the work at a later date.

### Educate your consumers

Part of your job is helping people understand how to interpret or draw
conclusions from your analysis. Provide the context and the full picture
around what you are doing, and not just the number that was being asked.

### Be both skeptic and champion

When looking at a result, always try to answer two questions:

- what other data could I gather to confirm this result?
- what could I find that would invalidate this result?

Doing so will help avoid blinding yourself to an early successful result
and make you improve more over time.

### Share with peers first, external consumers second

Essentially, double-check with your team to make sure you didn't miss
something.

### Expect and accept ignorance and mistakes

Proactively own up to your mistakes when you make them and find out, and
accept that sometimes you just can't answer the question at hand from
the available data. You want to be credible, and these things will make
you more credible to everyone involved.

[1]: http://www.unofficialgoogledatascience.com/2016/10/practical-advice-for-analysis-of-large.html
[2]: https://www.autodeskresearch.com/publications/samestats
