# Threats

In general, how can we identify thrests based on our data? We can of course use anomaly detection methods to surface potentially nefarious events or entities, but how can those be vetted to determine if they are an operational anomaly (e.g. hospital system with multiple logins around the same time) or a true anomaly. To make those distinctions, we will need to assess the SIGINT behind the anomaly itself. We'll stick with thinking about things at an event and an entity level.

## Event

- TLS/TCP inconsistencies with declared device
- Manipulation of declared user or device
- Rooted functionality used

## Entity

- Location inconsistencies (impossible rate of travel, rare country, GPS spoofing)
- Network inconsistencies (proxy indicators, aggregate events on an IP)
- Device behavior (event frequency from the same device)
- Network behavior (ASN/ISP/ORG source of operation's traffic)

## Appendix

OSI model:

- *Layer 10: Government or legal compliance*
- *Layer 9: Organization*
- *Layer 8: User or individual*
- Layer 7: Application
- Layer 6: Presentation
- Layer 5: Session
- Layer 4: Transport
- Layer 3: Network
- Layer 2: Data link
- Layer 1: Physical

TCP/IP model:

- Application (includes presentation, most of the session)
- TCP/IP transport
- Internet (network)
- Link (includes physical)
