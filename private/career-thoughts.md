# Career thoughts

Assembled from the [drunk][1] and [sober][2] Reddit posts and edited.
Some of my own commentary is included.

## Health

- Nothing can replace your mental health and happiness. Treat those as
  the most important, and if those choices aren't respected, you're in
  the wrong place.
- Be kind, both to others and to yourself. You are your harshest
  critic, but don't let that get in the way of your successes. Be proud
  of what you've accomplished.

## Staff engineering

Senior technical roles aren't described as much, but there are some
resources that capture that information, including [StaffEng][3]. There
is a book as well, which I may buy after reading some more on the
website first. Some general thoughts are captured below:

- A staff engineer could be "wide" and have support across a bunch of
  various subteams across the organization, or "deep" and focus on one
  or two areas. Examples of "wide" could be product design, while
  "deep" is more likely to focus on security. I would probably enjoy
  the latter, since it would help with focus and expertise.
- Staff engineers may have a few direct reports, but for the most part
  should not have many (or any).
- Finding *energizing* work will keep me in a job for a long time,
  where I am persuing impactful work. Bronwyn has also mentioned this
  for me to think about and follow.
- Impact might be measured more by who is doing the work that you're
  supporting.
- I think a tech lead or architect (see [StaffEng][4]) sounds
  interesting. I have the light experience of a technical lead from
  White Ops, which was both good and bad. At the time, since I had to
  be *both* a tech lead *and* a data scientist, that was one of the
  major underlying issues that led me to not enjoy that work as much.
- Take the time to understand the *status quo* before trying to make
  changes. Be diligent and deliberate, and you will get results.
- Deliberately accumulate expertise by doing valuable work. Focus on
  work that matters, do projects that develop you, and steer towards
  companies that value genuine experience.

## Job hunting

During my interview process at Duo, I asked most of the people I
interviewed with, “what would make you want to leave Duo?” The answers
I got were varied and honest, and I’d like to take the time to write
down what would be those triggering events for me at Duo. Much of this
will be influenced by my previous roles.

- Leadership that doesn’t respect or follow the company values
- Leadership that doesn’t reward research and development into new
  approaches, but instead focuses on “gamified” KPIs to a detrimental
  extent
- Signal collection apparatus used to support tracking, targeting, or
  other privacy-breaking activities on the internet
- Teammates that don’t support or help each other, or an overall
  feeling on competition or aggression between employees
- Lack of trust in the managerial relationship
- Decreasing acknowledgement or care about diversity and inclusion at
  the company
- Lack of humility in people following a mistake, or lack of
  psychological safety to be humble following a mistake
- Job function purely serves sales or marketing versus improving the
  product
- Lack of career improvement or growth potential

The above is subject to change over time, and not any single one of
those on its own would be enough to get me to leave. Likely, if things
turn for the worse like they did at White Ops/HUMAN, multiple points
above will expose themselves around the same time, or those that were
always present will become more pronounced.

Additionally, I still should consider a light job search after a few
years’ time to ensure that I am still learning and growing in my
career. At some point, I will need to consider what the next steps for
me are: manager or principal IC. I’ve been developing in both, and
could potentially step into either if necessary. Managerial work is a
different skill set, and I would need to work to improve those skills
in a directed fashion.


- If you're unsatisfied at your job, that's a sign you should start
  looking elsewhere.
- If people are assigning blame instead of figuring out the problem
  then fixing it, that's a sign you should look elsewhere.

## Productivity

- The amount of time worked has almost no bearing on the quality of the
  work. Most weeks, don't work close to 40 hours unless you feel like
  you are doing high-quality work. Take it easy on yourself.

## Python and coding

- Good code is simple, readable, and tested code. A student or new
  employee should be able to understand it. Better code is less code.
  The best code is no code at all. Find ways to reduce the amount of
  code you and everyone else writes.
- Use `mypy` for performance improvements if you don't already have
  highly-optimized code. For high computational code, use `numpy` and
  CUDA-supported frameworks.
- If you have a production pipeline, avoid `pandas` due to the high
  overhead in performance. Anything in this should either by in SQL or
  some scalable non-Python system if it needs a lot of transforms and
  such. Ideally, your "production" Python code will be purely model
  serving, or an offline framework that's been developed.

[1]: https://old.reddit.com/r/ExperiencedDevs/comments/nmodyl/drunk_post_things_ive_learned_as_a_sr_engineer/
[2]: https://old.reddit.com/r/ExperiencedDevs/comments/nnw7yd/sober_post_things_ive_learned_downleveling_my/
[3]: https://staffeng.com/
[4]: https://staffeng.com/guides/staff-archetypes
